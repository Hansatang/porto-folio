/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ["./src/**/*.{js,jsx,ts,tsx}"],
  theme: {
    extend: {},
  },
  plugins: [],
  prefix: "tw-",
  safelist: [{ pattern: /col/, variants: ["md", "xs", "xl"] }],
};
