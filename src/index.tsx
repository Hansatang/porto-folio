import React from "react";
import ReactDOM from "react-dom/client";
import "./index.css";
import reportWebVitals from "./reportWebVitals";
import About from "./components/About/About";
import "bootstrap/dist/css/bootstrap.min.css";
import "@fontsource/aileron/100.css";
import { createBrowserRouter, RouterProvider } from "react-router-dom";
import Layout from "./components/Layout/Layout";
import Main from "./components/Main/Main";
import Projects from "./components/Projects/Projects";
import Game from "./components/Game/Game";
import GameOfLife from "./components/GameOfLife/GameOfLife";

const router = createBrowserRouter([
  {
    path: "/",
    element: <Layout />,
    children: [
      {
        path: "/",
        element: <Main />,
      },
      {
        path: "about",
        element: <About />,
      },
      {
        path: "projects",
        element: <Projects />,
      },
      {
        path: "game",
        element: <Game />,
      },
      {
        path: "gameOfLife",
        element: <GameOfLife />,
      },
    ],
  },
]);

const root = ReactDOM.createRoot(
  document.getElementById("root") as HTMLElement,
);
root.render(
  <React.StrictMode>
    <RouterProvider router={router} />
  </React.StrictMode>,
);

reportWebVitals();
