import { useEffect, useRef } from "react";
import "./skills.scss";
import config from "../../environment/environment";

interface Position {
  x: number;
  y: number;
}

interface Velocity {
  dx: number;
  dy: number;
}

interface SkillBox {
  element: HTMLDivElement;
  position: Position;
  velocity: Velocity;
}

const Skills = () => {
  const skillContainerRef = useRef<HTMLDivElement>(null);
  const skillBoxesRef = useRef<Array<HTMLDivElement | null>>([]);

  const getCollisionVelocity: (
    cur: SkillBox,
    next: SkillBox,
    nextPos: Position,
  ) => Velocity = (cur: SkillBox, next: SkillBox, nextPos: Position) => {
    let { dx, dy } = cur.velocity;

    if (
      nextPos.x < next.position.x &&
      next.position.x <=
        nextPos.x + cur.element.getBoundingClientRect().width &&
      nextPos.y < next.position.y &&
      next.position.y < nextPos.y + cur.element.getBoundingClientRect().height
    ) {
      if (
        cur.position.y + cur.element.getBoundingClientRect().height <
        next.position.y
      ) {
        if (cur.velocity.dx === 1 && cur.velocity.dy === 1) {
          return { dx: 1, dy: -1 };
        } else {
          return {
            dx: -1,
            dy: -1,
          };
        }
      } else {
        if (cur.velocity.dx === 1 && cur.velocity.dy === 1) {
          return {
            dx: -1,
            dy: 1,
          };
        } else {
          return {
            dx: -1,
            dy: -1,
          };
        }
      }
    } else if (
      nextPos.x < next.position.x &&
      next.position.x < nextPos.x + cur.element.getBoundingClientRect().width &&
      nextPos.y <
        next.position.y + next.element.getBoundingClientRect().height &&
      next.position.y + next.element.getBoundingClientRect().height <
        nextPos.y + cur.element.getBoundingClientRect().height
    ) {
      if (
        cur.position.y >
        next.position.y + next.element.getBoundingClientRect().height
      ) {
        if (cur.velocity.dx === 1 && cur.velocity.dy === -1) {
          return {
            dx: 1,
            dy: 1,
          };
        } else {
          return { dx: -1, dy: 1 };
        }
      } else {
        if (cur.velocity.dx === 1 && cur.velocity.dy === -1) {
          return {
            dx: -1,
            dy: -1,
          };
        } else {
          return {
            dx: -1,
            dy: 1,
          };
        }
      }
    } else if (
      nextPos.x <
        next.position.x + next.element.getBoundingClientRect().width &&
      next.position.x + next.element.getBoundingClientRect().width <
        nextPos.x + cur.element.getBoundingClientRect().width &&
      nextPos.y < next.position.y &&
      next.position.y < nextPos.y + cur.element.getBoundingClientRect().height
    ) {
      if (
        cur.position.y + cur.element.getBoundingClientRect().height <
        next.position.y
      ) {
        if (cur.velocity.dx === -1 && cur.velocity.dy === 1) {
          return {
            dx: -1,
            dy: -1,
          };
        } else {
          return {
            dx: 1,
            dy: -1,
          };
        }
      } else {
        if (cur.velocity.dx === -1 && cur.velocity.dy === 1) {
          return {
            dx: 1,
            dy: 1,
          };
        } else {
          return { dx: 1, dy: -1 };
        }
      }
    } else if (
      nextPos.x <
        next.position.x + next.element.getBoundingClientRect().width &&
      next.position.x + next.element.getBoundingClientRect().width <
        nextPos.x + cur.element.getBoundingClientRect().width &&
      nextPos.y <
        next.position.y + next.element.getBoundingClientRect().height &&
      next.position.y + next.element.getBoundingClientRect().height <
        nextPos.y + cur.element.getBoundingClientRect().height
    ) {
      if (
        cur.position.y >
        next.position.y + next.element.getBoundingClientRect().height
      ) {
        if (cur.velocity.dx === -1 && cur.velocity.dy === -1) {
          return { dx: -1, dy: 1 };
        } else {
          return { dx: 1, dy: 1 };
        }
      } else {
        if (cur.velocity.dx === -1 && cur.velocity.dy === -1) {
          return {
            dx: 1,
            dy: -1,
          };
        } else {
          return {
            dx: 1,
            dy: 1,
          };
        }
      }
    }
    return { dx, dy };
  };

  useEffect(() => {
    const skillContainer = skillContainerRef.current;
    const skillBoxes: SkillBox[] = [];
    const skillRef = skillBoxesRef.current;
    if (!skillContainer) return;

    const skillContainerRect = skillContainer.getBoundingClientRect();

    const initializeSkills = () => {
      skillRef.forEach((skillBoxElement, index) => {
        if (!skillBoxElement) return;

        const skillBoxRect = skillBoxElement.getBoundingClientRect();
        let initialX, initialY;
        let isOverlapping;

        do {
          isOverlapping = false;
          initialX =
            Math.random() * (skillContainerRect.width - skillBoxRect.width);
          initialY =
            Math.random() * (skillContainerRect.height - skillBoxRect.height);

          for (let i = 0; i < index; i++) {
            const otherPosition = skillBoxes[i].position;
            const otherSkillBox = skillBoxes[i].element;

            if (!otherSkillBox) continue;

            const otherSkillBoxRect = otherSkillBox.getBoundingClientRect();

            if (
              initialX < otherPosition.x + otherSkillBoxRect.width &&
              initialX + skillBoxRect.width > otherPosition.x &&
              initialY < otherPosition.y + otherSkillBoxRect.height &&
              initialY + skillBoxRect.height > otherPosition.y
            ) {
              isOverlapping = true;
              break;
            }
          }
        } while (isOverlapping);

        skillBoxElement.style.left = initialX + "px";
        skillBoxElement.style.top = initialY + "px";

        skillBoxes.push({
          element: skillBoxElement,
          position: { x: initialX, y: initialY },
          velocity: {
            dx: Math.random() > 0.5 ? 1 : -1,
            dy: Math.random() > 0.5 ? 1 : -1,
          },
        });
      });
    };

    const getNextPos = (c: Position, v: Velocity) =>
      ({
        x: c.x + v.dx,
        y: c.y + v.dy,
      }) as Position;

    const moveSkillBoxes = () => {
      skillBoxes.forEach((skillBox, index) => {
        if (!skillBox || !skillBox.element) return;

        const nextPos = getNextPos(skillBox.position, skillBox.velocity);

        let curVelocity: Velocity = {
          dx: skillBox.velocity.dx,
          dy: skillBox.velocity.dy,
        };

        if (
          nextPos.x <= 0 ||
          nextPos.x + skillBox.element.offsetWidth >=
            skillContainerRect.width ||
          nextPos.y <= 0 ||
          nextPos.y + skillBox.element.offsetHeight >= skillContainerRect.height
        ) {
          if (
            nextPos.x <= 0 ||
            nextPos.x + skillBox.element.offsetWidth >= skillContainerRect.width
          ) {
            skillBox.velocity.dx *= -1;
          }

          if (
            nextPos.y <= 0 ||
            nextPos.y + skillBox.element.offsetHeight >=
              skillContainerRect.height
          ) {
            skillBox.velocity.dy *= -1;
          }
          return;
        }

        for (let i = 0; i < skillBoxes.length; i++) {
          if (i !== index && skillBox.element && skillBoxes[i].element) {
            curVelocity = getCollisionVelocity(
              skillBox,
              skillBoxes[i],
              nextPos,
            );

            if (
              skillBox.velocity.dx !== curVelocity.dx ||
              skillBox.velocity.dy !== curVelocity.dy
            ) {
              skillBox.velocity.dx = curVelocity.dx;
              skillBox.velocity.dy = curVelocity.dy;
              // TODO Change the collision logic to overlap logic
              // skillBox.position = { x: nextPos.x, y: nextPos.y };
              // skillBox.element.style.left = skillBox.position.x + "px";
              // skillBox.element.style.top = skillBox.position.y + "px";
              return;
            }
          }
        }

        skillBox.position = { x: nextPos.x, y: nextPos.y };
        skillBox.element.style.left = skillBox.position.x + "px";
        skillBox.element.style.top = skillBox.position.y + "px";
      });
    };

    initializeSkills();
    const animationId = setInterval(moveSkillBoxes, 30);

    const resizeObserver = new ResizeObserver((entries) => {
      const newSkillsBoxRect = entries[0].contentRect;

      skillBoxes.forEach((skillBox) => {
        if (!skillBox || !skillBox.element) return;

        if (
          skillBox.position.x + skillBox.element.offsetWidth >
            newSkillsBoxRect.width ||
          skillBox.position.y + skillBox.element.offsetHeight >
            newSkillsBoxRect.height
        ) {
          const newX = Math.min(
            skillBox.position.x,
            newSkillsBoxRect.width - skillBox.element.offsetWidth,
          );
          const newY = Math.min(
            skillBox.position.y,
            newSkillsBoxRect.height - skillBox.element.offsetHeight,
          );

          skillBox.element.style.left = newX + "px";
          skillBox.element.style.top = newY + "px";
          skillBox.position = { x: newX, y: newY };
        }
      });

      skillContainerRect.width = newSkillsBoxRect.width;
      skillContainerRect.height = newSkillsBoxRect.height;
    });

    resizeObserver.observe(skillContainer);

    return () => {
      clearInterval(animationId);
      resizeObserver.disconnect();
    };
  }, []);

  return (
    <div ref={skillContainerRef} className="skills-container">
      {config.skills.map((skill, index) => (
        <div
          key={index}
          ref={(ref) => (skillBoxesRef.current[index] = ref)}
          className="skill-box"
        >
          <p className="skill-text">{skill}</p>
        </div>
      ))}
    </div>
  );
};

export default Skills;
