import { Col } from "react-bootstrap";
import "./about.scss";
import Skills from "../Skills/Skills";
import config from "../../environment/environment";
import EducationCard from "../EducationCard/EducationCard";
import ExperienceCard from "../ExperienceCard/ExperienceCard";

function About() {
  const experiences = config.experiences.map((e, idx) => (
    <ExperienceCard key={idx} experience={e} />
  ));
  const educations = config.education.map((e, idx) => (
    <EducationCard key={idx} education={e} />
  ));

  return (
    <Col className="about">
      <div className="about-tile-row">
        <div className="px-5 py-5">
          <h3 className="mb-2 mt-0 text-3xl font-medium leading-tight">
            About Me
          </h3>
          <div className="tw-border-t tw-border-gray-200" />
          <br />
          {config.aboutParagraphs.map((aboutText) => (
            <div className="mb-2 mt-0 tw-text-justify text-base font-medium leading-tight">
              {aboutText}
            </div>
          ))}

          <div className="py-3">
            <h3 className="mb-2 mt-0 text-3xl font-medium leading-tight">
              Experience
            </h3>
            <div className="tw-border-t tw-border-gray-200" />
            <div className="tw-py-6 tw-grid tw-grid-cols-1 xl:tw-grid-cols-2 lg:tw-flex-row">
              {experiences}
            </div>
          </div>
          <div className="py-3">
            <h3 className="mb-2 mt-0 text-3xl font-medium leading-tight">
              Skills
            </h3>
            <div className="tw-border-t tw-border-gray-200" />
            <Skills />
          </div>
          <div className="py-3">
            <h3 className="mb-2 mt-0 text-3xl font-medium leading-tight">
              Education
            </h3>
            <div className="tw-border-t tw-border-gray-200" />
            <div className="tw-py-6 tw-grid tw-grid-cols-1 xl:tw-grid-cols-2 lg:tw-flex-row">
              {educations}
            </div>
          </div>
        </div>
      </div>
    </Col>
  );
}

export default About;
