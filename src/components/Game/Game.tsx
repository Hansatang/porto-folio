import { useState, useEffect } from "react";
import { Unity, useUnityContext } from "react-unity-webgl";
import { Col } from "react-bootstrap";
import { useBlocker } from "react-router-dom";
import "./game.scss";

const Game = () => {
  const unityContext = useUnityContext({
    loaderUrl: "unityBuild/MatheMagician.loader.js",
    dataUrl: "unityBuild/MatheMagician.data",
    frameworkUrl: "unityBuild/MatheMagician.framework.js",
    codeUrl: "unityBuild/MatheMagician.wasm",
  });

  const [width, setWidth] = useState<number>(window.innerWidth);
  const isMobile = width <= 768;
  const blocker = useBlocker(true);

  if (blocker.state === "blocked") {
    if (isMobile) {
      blocker.proceed();
    } else {
      unityContext.unload().then(() => blocker.proceed());
    }
  }

  if (isMobile) {
    unityContext.unload();
  }

  function handleWindowSizeChange() {
    setWidth(window.innerWidth);
  }

  useEffect(() => {
    window.addEventListener("resize", handleWindowSizeChange);

    return () => window.removeEventListener("resize", handleWindowSizeChange);
  }, []);

  function gameFullscreen() {
    unityContext.requestFullscreen(true);
  }

  return (
    <Col className="content">
      <div className="game-tile-row">
        <div className="game-container-padding">
          {isMobile ? (
            <div className="text-center">
              <h6>Unfortunately the game doesn't support mobile devices</h6>
            </div>
          ) : (
            <div>
              <div className="game-fullscreen-button-container">
                <button
                  className="game-fullscreen-button"
                  onClick={() => gameFullscreen()}
                >
                  Fullscreen
                </button>
              </div>
              <Unity
                style={{
                  width: "100%",
                  justifySelf: "center",
                  cursor: "pointer",
                }}
                unityProvider={unityContext.unityProvider}
              />
            </div>
          )}
        </div>
      </div>
    </Col>
  );
};

export default Game;
