import "./aboutCard.scss";

export type AboutCardProps = {
  imageSrc: string;
  altText: string;
  title: string;
  dateRange: string;
  subtitle: string;
  responsibilities: string[];
};

function AboutCard(props: AboutCardProps) {
  return (
    <div className="job-experience px-1 py-1">
      <img src={props.imageSrc} alt={props.altText} className="image-sizing" />
      <br />
      <h5>{props.title}</h5>
      <h6>{props.dateRange}</h6>
      <h6>{props.subtitle}</h6>
      <ul className="px-0">
        {props.responsibilities.map((responsibility, index) => (
          <li key={index}>• {responsibility}</li>
        ))}
      </ul>
    </div>
  );
}

export default AboutCard;
