import Education from "../../domain/education";
import AboutCard from "../AboutCard/AboutCard";

export type EducationCardProps = {
  education: Education;
};

function EducationCard(props: EducationCardProps) {
  return (
    <AboutCard
      imageSrc={props.education.imageSrc}
      altText={props.education.altText}
      title={props.education.facility}
      dateRange={props.education.dateRange}
      subtitle={props.education.program}
      responsibilities={props.education.skills}
    />
  );
}

export default EducationCard;
