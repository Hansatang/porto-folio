import { Outlet } from "react-router-dom";
import Sidebar from "../Sidebar/Sidebar";
import "./layout.scss";
import { Container, Row } from "react-bootstrap";

function Layout() {
  return (
    <Container fluid className="overflow-hidden main-background">
      <Row className="layout-row">
        <Sidebar />
        <Outlet />
      </Row>
    </Container>
  );
}

export default Layout;
