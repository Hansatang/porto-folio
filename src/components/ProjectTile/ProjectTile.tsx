import "./projectTile.scss";
import Project from "../../domain/project";

export type ProjectTileProps = {
  project: Project;
  textWidth?: number;
};

function ProjectTile(props: ProjectTileProps) {
  const handleRepositoryClick = () => {
    window.open(props.project.repository, "_blank");
  };

  const rowSize = 6;

  const textColWidth = props.textWidth ?? rowSize;
  const imgColWidth = props.textWidth ? rowSize - textColWidth : rowSize;

  const textColStyle = `md:tw-pr-6 tw-mb-6 md:tw-mb-0 tw-col-span-${textColWidth}`;
  const imgColStyle = `tw-flex tw-items-start tw-justify-center tw-col-span-${imgColWidth}`;

  return (
    <div className="tw-border-t tw-border-gray-200">
      <div
        className={`tw-py-6 tw-grid tw-grid-cols-1 xl:tw-grid-cols-${rowSize} lg:tw-flex-row`}
      >
        <div className={textColStyle}>
          <h1 className="tw-text-xl tw-text-justify tw-font-semibold tw-mb-2">
            {props.project.header}
          </h1>
          <h4 className="tw-text-justify">{props.project.title}</h4>
          <p className="tw-text-gray-700 tw-text-justify tw-mb-4">
            {props.project.text}
          </p>
          <div className="tw-mb-4">
            <p className="tw-text-gray-700">Technologies used:</p>
            <div className="tw-flex tw-flex-wrap">
              <div className="tw-mr-4">
                <p className="tw-text-gray-700">Backend:</p>
                <ul>
                  {props.project.backend.map((tech, index) => (
                    <li key={index} className="tw-text-gray-700">
                      {tech}
                    </li>
                  ))}
                </ul>
              </div>
              <div>
                <p className="tw-text-gray-700">Frontend:</p>
                <ul>
                  {props.project.frontend.map((tech, index) => (
                    <li key={index} className="tw-text-gray-700">
                      {tech}
                    </li>
                  ))}
                </ul>
              </div>
            </div>
          </div>
          <button className="buttonStyles" onClick={handleRepositoryClick}>
            Repository
          </button>
        </div>
        <div className={imgColStyle}>
          <img
            src={props.project.image}
            alt={props.project.title}
            className="projectImage"
          />
        </div>
      </div>
    </div>
  );
}

export default ProjectTile;
