import { NavLink } from "react-router-dom";
import "./customNavLink.scss";

export type NavLinkProps = {
  to: string;
  children: string;
};

function CustomNavLink(props: NavLinkProps) {
  const linkStyle = ({ isActive }: { isActive: boolean }) => ({
    color: isActive ? "yellow" : "white",
  });

  return (
    <NavLink to={props.to} className="custom-link" style={linkStyle}>
      {props.children}
    </NavLink>
  );
}

export default CustomNavLink;
