import ProjectTile from "../ProjectTile/ProjectTile";
import "./projects.scss";
import { Col } from "react-bootstrap";
import config from "../../environment/environment";

function Projects() {
  const textWidths = [4, undefined, 4];

  return (
    <Col className="projects">
      <div className="project-tile-row">
        <div className="px-5 py-5">
          <h3 className="mb-2 mt-0 text-3xl font-medium leading-tight">
            Projects
          </h3>
          <h6 className="mb-2 mt-0 text-base font-medium leading-tight">
            Below are some of my projects, that I'm especially proud of.
          </h6>
          {config.projects.map((p, idx) => (
            <ProjectTile key={idx} project={p} textWidth={textWidths[idx]} />
          ))}
        </div>
      </div>
    </Col>
  );
}

export default Projects;
