import Experience from "../../domain/experience";
import AboutCard from "../AboutCard/AboutCard";

export type ExperienceCardProps = {
  experience: Experience;
};

function ExperienceCard(props: ExperienceCardProps) {
  return (
    <AboutCard
      imageSrc={props.experience.imageSrc}
      altText={props.experience.altText}
      title={props.experience.title}
      dateRange={props.experience.dateRange}
      subtitle={props.experience.subtitle}
      responsibilities={props.experience.responsibilities}
    />
  );
}

export default ExperienceCard;
