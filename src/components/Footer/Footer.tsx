import React, { ReactNode } from "react";
import { Col, Container, Row } from "react-bootstrap";

function Footer() {
  return (
    <Container fluid>
      <Row className="fixed-bottom">
        <Col>
          <div className="social-text reactive-hide">Get in touch</div>
          <div className="social-content">
            <div className="social-icons">
              <a
                rel="me"
                href="https://github.com/Hansatang?tab=repositories"
                className="button button-icon u-email"
                title="Github"
              >
                <div>Github</div>
              </a>
            </div>
          </div>
        </Col>
      </Row>
    </Container>
  );
}

export default Footer;
