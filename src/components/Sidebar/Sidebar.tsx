import "./sidebar.scss";
import CustomNavLink from "../CustomNavLink/CustomNavLink";
import { Col, NavDropdown, Navbar, Nav } from "react-bootstrap";
import { useState } from "react";
import PersonalCard from "../PersonalCard/PersonalCard";
import SocialCard from "../SocialCard/SocialCard";

function Sidebar() {
  const [expanded, setExpanded] = useState(false);

  return (
    <Col md={3} xs={12} className="sidebar-background">
      <div className="sidebar">
        <div>
          <PersonalCard />
          <div className="navigation">
            <Navbar
              expand="md"
              variant="dark"
              className="d-md-none"
              expanded={expanded}
              onToggle={() => setExpanded(!expanded)}
            >
              <NavDropdown
                title="Menu"
                id="basic-nav-dropdown"
                data-bs-theme="dark"
              >
                <NavDropdown.Item>
                  <CustomNavLink to="/">Home </CustomNavLink>
                </NavDropdown.Item>
                <NavDropdown.Divider />
                <NavDropdown.Item>
                  <CustomNavLink to="/about">About </CustomNavLink>
                </NavDropdown.Item>
                <NavDropdown.Item>
                  <CustomNavLink to="/projects">Projects </CustomNavLink>
                </NavDropdown.Item>
                <NavDropdown.Item>
                  <CustomNavLink to="/game">Game </CustomNavLink>
                </NavDropdown.Item>
                <NavDropdown.Item>
                  <CustomNavLink to="/gameOfLife">
                    Conway's Game of Life
                  </CustomNavLink>
                </NavDropdown.Item>
              </NavDropdown>
            </Navbar>
            <Nav className="flex-column d-none d-md-block">
              <CustomNavLink to="/">Home </CustomNavLink>
              <CustomNavLink to="/about">About </CustomNavLink>
              <CustomNavLink to="/projects">Projects </CustomNavLink>
              <CustomNavLink to="/game">Game </CustomNavLink>
              <CustomNavLink to="/gameOfLife">
                Conway's Game of Life
              </CustomNavLink>
            </Nav>
          </div>
        </div>
        <SocialCard />
      </div>
    </Col>
  );
}

export default Sidebar;
