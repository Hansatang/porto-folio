import {
  ChangeEventHandler,
  useCallback,
  useEffect,
  useMemo,
  useState,
} from "react";
import GameModel, { Grid } from "../../domain/GameOfLife";
import { Col } from "react-bootstrap";
import "./GameOfLife.scss";

function GameOfLife() {
  const numRows: number = 25;
  const numCols: number = 25;

  const game = useMemo<GameModel>(
    () => new GameModel(numRows, numCols),
    [numRows, numCols]
  );
  const [grid, setGrid] = useState<Grid>(game.grid);
  const [running, setRunning] = useState<boolean>(false);
  const [speed, setSpeed] = useState<number>(1000);

  const handleCellClick = (x: number, y: number): void => {
    game.toggleCell(x, y);
    setGrid([...game.grid]);
  };

  const next = useCallback((): void => {
    game.next();
    setGrid([...game.grid]);
  }, [game]);

  const changeState = (): void => {
    setRunning((prev) => !prev);
  };

  const changeSpeed: ChangeEventHandler<HTMLInputElement> = (e): void => {
    setSpeed(parseInt(e.target.value));
  };

  useEffect(() => {
    let interval: ReturnType<typeof setInterval>;
    if (running) {
      interval = setInterval(() => next(), speed);
    }
    return () => clearInterval(interval);
  }, [running, next, speed]);

  useEffect(() => {
    game.populateRandom();
    setGrid([...game.grid]);
  }, [game]);

  return (
    <Col className="content">
      <h1>GAME OF LIFE</h1>
      <div className="boardContainer" style={{ width: `${numCols * 20}px` }}>
        {grid.map((row, r) =>
          row.map((cell, c) => (
            <div
              key={`${r} ${c}`}
              onClick={() => handleCellClick(r, c)}
              style={{
                width: 20,
                height: 20,
                border: "1px solid black",
                backgroundColor: cell ? "black" : "white",
              }}
            ></div>
          ))
        )}
        <button className="game-button" onClick={next}>
          Next
        </button>

        <button className="game-button" onClick={changeState}>
          {running ? "Pause" : "Start"}
        </button>

        <input
          className="game-button"
          type="number"
          value={speed}
          onChange={changeSpeed}
        />
      </div>
    </Col>
  );
}

export default GameOfLife;
