import "./personalCard.scss";

function PersonalCard() {
  return (
    <div className="personal-card reactive-hide">
      <a href="/" className="personal-info">
        <img
          src={require("../../assets/images/myPhoto.jpg")}
          alt="me"
          className="avatar"
        />
        <div className="personal-content ">
          <h1 className="personal-name">Krzysztof Pacierz</h1>
          <h2 className="personal-title">Software Developer</h2>
        </div>
      </a>
    </div>
  );
}

export default PersonalCard;
