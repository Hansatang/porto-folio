import React from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faGitlab, faLinkedinIn } from "@fortawesome/free-brands-svg-icons";
import { faEnvelope } from "@fortawesome/free-solid-svg-icons";
import "./socialCard.scss";

function SocialCard() {
  return (
    <div className="social">
      <div className="social-text reactive-hide">Get in touch</div>
      <div className="social-content">
        <a
          rel="noopener noreferrer"
          target="_blank"
          href="mailto:krzysztof.pacierz@gmail.com"
          className="button button-icon u-email"
          title="Email"
        >
          <FontAwesomeIcon icon={faEnvelope} />
        </a>
        <a
          rel="noopener noreferrer"
          target="_blank"
          href="https://gitlab.com/Hansatang"
          className="button button-icon u-email"
          title="Gitlab"
        >
          <FontAwesomeIcon icon={faGitlab} />
        </a>
        <a
          rel="noopener noreferrer"
          target="_blank"
          href="www.linkedin.com/in/krzysztof-pacierz"
          className="button button-icon u-email"
          title="Linkedin"
        >
          <FontAwesomeIcon icon={faLinkedinIn} />
        </a>
      </div>
    </div>
  );
}

export default SocialCard;
