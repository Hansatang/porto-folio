import { Col } from "react-bootstrap";
import "./main.scss";

function Main() {
  return (
    <Col className="content">
      <div className="main-header px-2 py-2">
        <div className="home-header">Hi, I'm Krzysztof.</div>

        <div className="home-subheader">
          A software engineer with burning passion in{" "}
          <b>front-end and back-end</b>.
        </div>
        <div className="home-subheader">
          Proficient with machine learning solutions.
        </div>
      </div>
    </Col>
  );
}

export default Main;
