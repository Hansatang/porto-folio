import Education from "../domain/education";
import Project from "../domain/project";

const projects: Project[] = [
  {
    header:
      "Similarity detection between inputs with use of Siamese Neural Network",
    title: "Bachelor project in cooperation with Stibo Accelerator",
    text: "This bachelor project explores the use of machine learning in duplicate detection problem for images.The goal is to create a system able to process vectorized images and determine similarity of suspected duplicates using a Siamese Neural Network model.",
    backend: ["Python", "FastAPI", "Tensorflow with Keras"],
    frontend: ["React", "TypeScript", "Bootstrap"],
    image: require("../assets/images/Bachelor.jpg"),
    repository: "https://gitlab.com/bachelor2681836",
  },
  {
    header: "IMDB Clone",
    title:
      "Semester project for Via University, emulating real life movie site",
    text: "The objective of this project is to create a cloud-based web application catering to the needs of movie enthusiasts, by offering convenient access to comprehensive information on actors and movies.",
    backend: [
      "Java",
      "Spring Framework",
      "Google Cloud Platform",
      "PostgreSQL",
      "Gitlab CI/CD",
      "Docker",
    ],
    frontend: ["React", "Typescript", "Gitlab CI/CD", "Docker"],
    image: require("../assets/images/IMDBClone.jpg"),
    repository: "https://gitlab.com/sep6-movies",
  },
  {
    header: "Air4You",
    title: "Via University project about detecting air quality by sensors",
    text: "Project was created to monitor house air conditions. Environmental factors like temperature, air humidity and carbon dioxide are being read from the sensors, and present to the user throught the Android App. Project can also provide customers with statistical data of taken measurements, like historical data or thresholds alerts.",
    backend: [
      "Java",
      "C",
      "FreeRTOS",
      "REST",
      "API",
      "JPA",
      "Spring Framework",
    ],
    frontend: ["Android", "PowerBI"],
    image: require("../assets/images/Air4You.jpg"),
    repository: "https://gitlab.com/Hansatang/Sep4Android",
  },
];

const aboutParagraphs = [
  "Hi there! I'm Krzysztof Pacierz, a Software Engineer driven by a passion for crafting robust and efficient solutions. Armed with a Bachelor's degree in Software Engineering from VIA University College, I've cultivated a solid foundation in software development and a relentless pursuit of excellence.",
  "My professional experience at STIBO and SYSTEMATIC A/S has equipped me with practical skills in data manipulation, machine learning, and frontend development. Through collaborative efforts, I've contributed to projects focused on enhancing system reliability and user experience. ",
  "My approach to software engineering is pragmatic and results-oriented. I thrive in environments that prioritize clear communication, meticulous planning, and agile methodologies. From implementing complex algorithms to designing intuitive interfaces, I am committed to delivering high-quality solutions that meet both technical requirements and user expectations.",
  "Beyond the confines of coding, I enjoy engaging in intellectually stimulating activities such as board games and philosophical discussions. These interests fuel my creativity and foster a well-rounded perspective that enriches my work.",
  "As I continue to grow professionally, I remain dedicated to honing my skills, embracing new challenges, and making meaningful contributions to impactful projects. I am excited about the opportunities that lie ahead and eager to leverage my expertise to drive innovation in the field of software engineering. ",
];

const skills = [
  "Java",
  "Python",
  ".Net",
  "Selenium",
  "Git",
  "React",
  "Vue.js",
  "SQL",
  "Keras",
  "Android",
  "SCSS",
  "JUnit",
  "Angular",
  "JQuery",
  "CI/CD",
];

const experiences = [
  {
    imageSrc: require("../assets/icons/Stibo-Accelerator.jpg"),
    altText: "Job",
    title: "Stibo Accelerator",
    dateRange: "06/2023 - 01/2024",
    subtitle: "Colaboration on Bachelor Thesis",
    responsibilities: [
      "Data Cleaning and Transformation",
      "Implementing Siamese Neural Networks capable of capturing similarity between input pairs",
      "CI/CD and React frontend for machine learning models",
    ],
  },
  {
    imageSrc: require("../assets/icons/Systematic.jpg"),
    altText: "Job",
    title: "SYSTEMATIC A/S",
    dateRange: "08/2022 - 01/2023",
    subtitle: "Junior Software Developer",
    responsibilities: [
      "Maintaining hospitals databases through migration scripts",
      "Implementing user friendly frontend using Windows Forms .NET",
      "Designing and deploying complex CI/CD solutions (enhanced with custom bash scripts)",
    ],
  },
];

const education: Education[] = [
  {
    imageSrc: require("../assets/icons/Via.jpg"),
    altText: "University",
    facility: "Via University College",
    dateRange: "08/2020 - 01/2024",
    program: "ICT Software Engineering, VIA Horsens",
    skills: [
      "System architecture",
      "Agile driven development",
      "Full-stack Software Engineering",
    ],
  },
];

const config = { projects, aboutParagraphs, skills, experiences, education };

export default config;
