interface Project {
  header: string;
  title: string;
  text: string;
  textWidth?: number;
  frontend: string[];
  backend: string[];
  image: string;
  repository: string;
}

export default Project;
