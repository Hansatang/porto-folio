interface Experience {
  imageSrc: string;
  altText: string;
  title: string;
  dateRange: string;
  subtitle: string;
  responsibilities: string[];
}

export default Experience;
