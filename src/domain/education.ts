interface Education {
  imageSrc: string;
  altText: string;
  facility: string;
  dateRange: string;
  program: string;
  skills: string[];
}

export default Education;
