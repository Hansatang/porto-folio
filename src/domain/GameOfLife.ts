export type Grid = boolean[][];

class GameModel {
  numRows: number;
  numCols: number;
  grid: Grid;
  nextGrid: Grid;
  constructor(x: number, y: number) {
    this.numRows = x;
    this.numCols = y;
    this.grid = this.createEmptyGrid(x, y);
    this.nextGrid = this.createEmptyGrid(x, y);
  }

  toggleCell(x: number, y: number) {
    this.grid[x][y] = !this.grid[x][y];
  }

  createEmptyGrid(x: number, y: number) {
    let result: Grid = [];
    for (let i = 0; i < x; i++) {
      result[i] = [];
      for (let j = 0; j < y; j++) {
        result[i][j] = false;
      }
    }
    return result;
  }

  deepCopy(arr: Grid, next: Grid) {
    for (let i = 0; i < this.numRows; i++) {
      for (let j = 0; j < this.numCols; j++) {
        arr[i][j] = next[i][j];
      }
    }
  }

  checkCell(x: number, y: number) {
    let neig = 0;
    for (let i = -1; i <= 1; i++) {
      for (let j = -1; j <= 1; j++) {
        if (i === 0 && j === 0) continue;
        let nextX = (x + i + this.numRows) % this.numRows;
        let nextY = (y + j + this.numCols) % this.numCols;

        if (this.grid[nextX][nextY]) {
          neig++;
        }
      }
    }

    if (this.grid[x][y]) {
      return neig === 2 || neig === 3;
    } else {
      return neig === 3;
    }
  }

  updateCell(x: number, y: number) {
    this.nextGrid[x][y] = this.checkCell(x, y);
  }

  next() {
    this.grid.forEach((row, r) =>
      row.forEach((cell, c) => this.updateCell(r, c)),
    );
    this.deepCopy(this.grid, this.nextGrid);
  }

  populateRandom() {
    for (let i = 0; i < this.numRows; i++) {
      for (let j = 0; j < this.numCols; j++) {
        this.grid[i][j] = Math.random() > 0.5;
      }
    }
  }
}

export default GameModel;
